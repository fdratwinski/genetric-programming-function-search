#pragma once
#include "constants.h"
#include <string>
using namespace std;

int parseToInt(string number);
double parseToDouble(string number);
bool isNumber(string line);
string parseToString(int number);