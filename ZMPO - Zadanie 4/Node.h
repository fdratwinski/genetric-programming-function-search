#pragma once
#include <string>
#include <vector>
using namespace std;
class CNode
{
	friend class CTree;

public:
	CNode();

	CNode(string v, vector<CNode*> children, int nodeType);

	CNode(string v, int nodeType);

	CNode(CNode &otherNode);

	~CNode();


private:
	vector<CNode*> tableOfChildren;
	string value;
	int typeOfNode;
};

