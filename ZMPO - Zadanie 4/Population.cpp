#include "Population.h"

CPopulation::CPopulation(int numberOfPop,int numberOfIters, int percentageOfMutate, int percentageOfPairing)
{
	numberOfPopulation = numberOfPop;
	numberOfIterations = numberOfIters;
	possibiltyOfMutation = percentageOfMutate;
	possibiltyOfBeingPaired = percentageOfPairing;
}

CPopulation::~CPopulation()
{
	for (int i = 0; i < populationOfTrees.size(); i++)
	{
		delete populationOfTrees[i];
	}
	for (int i = 0; i < populationOfParents.size(); i++)
	{
		delete populationOfParents[i];
	}
}


void CPopulation::findTheBestExpression(CDataManager &database)
{
	srand(time(NULL));

	for (int i = 0; i < numberOfPopulation; i++)
	{
		CTree* tree = new CTree();
		tree->buildARandomTree();
		while(tree->evaluationFunctionFromDatabase(database) == DIVIDING_BY_ZERO_ERROR_CODE)
		{
			tree->buildARandomTree();
		}
		populationOfTrees.push_back(tree);
	}

	for (int i = 0; i <numberOfIterations; i++)
	{
		selection();
		pair(database);
		mutation();
		evalution(database);
	}
	CTree* bestTree = new CTree(*populationOfTrees[0]);
	int indexOfBestTree = 0;
	for (int j = 0; j < populationOfTrees.size(); j++)
	{
		cout << populationOfTrees[j]->print() << endl;
		if (populationOfTrees[j]->getEvalutedExpression() < populationOfTrees[indexOfBestTree]->getEvalutedExpression())
		{
			indexOfBestTree = j;
		}
	}
	delete bestTree;
	bestTree = new CTree(*populationOfTrees[indexOfBestTree]);
	cout << bestTree->print();
	string bestTreeData = bestTree->print() + NEW_LINE + to_string(bestTree->getEvalutedExpression());
	database.saveToFile(bestTreeData);;
	delete bestTree;
	getchar();
}

void CPopulation::selection()
{
	for (int j = 0; j < numberOfPopulation; j++)
	{

		int indexOfFirstRandomTree = rand() % populationOfTrees.size();
		int indexOfSecondRandomTree = rand() % populationOfTrees.size();
		int whichOneIsWorse;
		CTree* betterTree = new CTree(tournament(*populationOfTrees[indexOfFirstRandomTree], *populationOfTrees[indexOfSecondRandomTree], whichOneIsWorse));
		if (whichOneIsWorse == FIRST_ONE_IS_WORSE)
		{
			delete populationOfTrees[indexOfFirstRandomTree];
			populationOfTrees.erase(populationOfTrees.begin() + indexOfFirstRandomTree);
		}
		else
		{
			delete populationOfTrees[indexOfSecondRandomTree];
			populationOfTrees.erase(populationOfTrees.begin() + indexOfSecondRandomTree);
		}
		populationOfParents.push_back(betterTree);
	}
	for (int j = 0; j < populationOfTrees.size(); j++)
		delete populationOfTrees[j];
	populationOfTrees.clear();
}

void CPopulation::pair(CDataManager &database)
{
	int numberOfIterationsForPairing = numberOfPopulation / 2;
	for (int j = 0; j < numberOfIterationsForPairing; j++)
	{
		int indexOfFirstRandomTree = rand() % populationOfParents.size();
		int indexOfSecondRandomTree = rand() % populationOfParents.size();
		int possibiltyOfBeingParied2 = rand() % 100;
		if (possibiltyOfBeingParied2 <= possibiltyOfBeingPaired)
			pair(*populationOfParents[indexOfFirstRandomTree], *populationOfParents[indexOfSecondRandomTree], database);
		else
		{
			populationOfTrees.push_back(new CTree(*populationOfParents[indexOfFirstRandomTree]));
			populationOfTrees.push_back(new CTree(*populationOfParents[indexOfSecondRandomTree]));
		}
	}
	for (int j = 0; j < populationOfParents.size(); j++)
		delete populationOfParents[j];
	populationOfParents.clear();
}

void CPopulation::mutation()
{
	for (int j = 0; j < populationOfTrees.size(); j++)
	{
		int possibilityOfMutate = rand() % 100;
		if (possibilityOfMutate <= possibiltyOfMutation)
		{
			populationOfTrees[j]->mutate();
		}
	}
}

void CPopulation::evalution(CDataManager &database)
{
	for (int j = 0; j < populationOfTrees.size(); j++)
	{
		while (populationOfTrees[j]->evaluationFunctionFromDatabase(database) == DIVIDING_BY_ZERO_ERROR_CODE)
		{
			populationOfTrees[j]->buildARandomTree();
		}
	}
}

void CPopulation::pair(CTree &firstTree, CTree &secondTree,CDataManager &database)
{
	CTree* firstChild = new CTree(firstTree);
	CTree* secondChild = new CTree(secondTree);
	string sequenceToFirstNode = EMPTY_FORMULA;
	string sequenceToSecondNode = EMPTY_FORMULA;
	CNode firstNode(*firstChild->chooseRandomNode(sequenceToFirstNode));
	CNode secondNode(*secondChild->chooseRandomNode(sequenceToSecondNode));

	firstChild->pairWithAnotherTree(sequenceToFirstNode,secondNode);
	secondChild->pairWithAnotherTree(sequenceToSecondNode,firstNode);
	populationOfTrees.push_back(firstChild);
	populationOfTrees.push_back(secondChild);
}

CTree& CPopulation::tournament(CTree &firstTree, CTree &secondTree, int &whichOneIsWorse)
{
	if (firstTree.getEvalutedExpression() > secondTree.getEvalutedExpression())
	{
		whichOneIsWorse = 0;
		return secondTree;
	}
	else
	{
		whichOneIsWorse = 1;
		return firstTree;
	}
}
