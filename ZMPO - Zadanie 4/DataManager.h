#pragma once
#include <string>
#include <fstream>
#include <vector>
#include <iostream>
#include "constants.h"
#include "tools.h"

using namespace std;

class CDataManager
{
	friend class CTree;
public:
	CDataManager();
	~CDataManager();
	int openFile(string namefile);
	void saveToFile(string data);
private:
	vector<vector<double>> arrayOfData;
};

