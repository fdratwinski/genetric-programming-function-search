#pragma once
#include "Tree.h"
#include "DataManager.h"
#include <vector>
#include <iostream>
#include <cstdlib>
#include <ctime>


using namespace std;

class CPopulation
{
public:
	CPopulation(int numberOfPop, int numberOfIters, int percentageOfMutate, int percentageOfPairing);
	~CPopulation();
	void findTheBestExpression(CDataManager &database);
	void mutation();
	void pair(CDataManager &database);
	void selection();
	void evalution(CDataManager &database);
private:
	void pair(CTree &firstTree, CTree &secondTree, CDataManager &database);
	CTree& tournament(CTree & firstTree, CTree & secondTree, int &whichOneIsWorse);
	vector<CTree*> populationOfTrees;
	vector<CTree*> populationOfParents;
	int numberOfPopulation;
	int numberOfIterations;
	int possibiltyOfMutation;
	int possibiltyOfBeingPaired;
};

