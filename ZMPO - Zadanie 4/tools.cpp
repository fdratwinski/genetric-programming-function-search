#include "tools.h"

int parseToInt(string number)
{
	int result = 0;
	for (int i = 0; i < number.length(); i++)
	{
		result *= PARSE_NEXT_DIGIT;
		result += (int)number.at(i) - ASCII_FIRST_DIGIT;
	}
	return result;
}

double parseToDouble(string number)
{
	double result = 0;
	int i = 0;
	if (number.at(i) == MINUS)i++;
	bool ifWholeNumbers = true;
	double counter = 1;
	for (; i < number.length(); i++)
	{
		if (number.at(i) == DOT)
		{
			ifWholeNumbers = false;
		}
		else if (ifWholeNumbers)
		{
			result *= 10;
			result += (double)number.at(i) - ASCII_FIRST_DIGIT;
		}
		else if (!ifWholeNumbers)
		{
			counter = counter / 10;
			result += ((double)number.at(i) - ASCII_FIRST_DIGIT) * counter;
		}
	}
	if (number.at(0) == MINUS) result = result* (-1);

	return result;
}


bool isNumber(string line)
{
	bool isNumber = line.length() != 0;
	for (int i = 0; i < line.length() && isNumber; i++)
	{
		char sign = line.at(i);
		if ((int)sign <ASCII_FIRST_DIGIT || (int)sign > ASCII_LAST_DIGIT)
			isNumber = false;
	}
	return isNumber;
}

string parseToString(int number)
{
	string result;
	while (number != 0)
	{
		char digit = number % PARSE_MOD + ASCII_FIRST_DIGIT;
		result = digit + result;
		number = number / PARSE_MOD;
	}
	return result;
}