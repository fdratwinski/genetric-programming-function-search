#include "DataManager.h"

CDataManager::CDataManager()
{
}


CDataManager::~CDataManager()
{
}

int CDataManager::openFile(string nameOfFile)
{
	ifstream myFile;
	myFile.open(nameOfFile);
	string line;
	if (myFile.is_open())
	{
		while (!myFile.eof())
		{
			getline(myFile, line);
			if (line.length() != 0)
			{
				vector<double> tempVector;
				for (int i = 0; i < line.length(); i++)
				{
					string number;
					while (i < line.length() && line.at(i) != SEMICOLON)
					{
						number += line.at(i);
						i++;
					}
					tempVector.push_back(parseToDouble(number));
				}
				arrayOfData.push_back(tempVector);
			}
		}
		myFile.close();
		return NO_ERROR;
	}
	else
		return ERROR_IN_OPENING_FILE;
}

void CDataManager::saveToFile(string data)
{
	ofstream fileWriter;
	fileWriter.open(NAME_OF_FILE_WITH_BEST_TREE);
	fileWriter.clear();
	fileWriter << data;
	fileWriter.close();
}
