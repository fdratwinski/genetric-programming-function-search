#include "Node.h"

CNode::CNode() {}

CNode::CNode(string v, vector<CNode*> children,int nodeType)
{
	typeOfNode = nodeType;
	tableOfChildren = children;
	value = v;
}

CNode::CNode(string v,int nodeType)
{
	value = v;
	typeOfNode = nodeType;
}

CNode::CNode(CNode &otherNode)
{
	value = otherNode.value;
	for (int i = 0; i < otherNode.tableOfChildren.size(); i++)
	{
		tableOfChildren.push_back(new CNode(*otherNode.tableOfChildren[i]));
	}
	typeOfNode = otherNode.typeOfNode;
}

CNode::~CNode()
{
	for (int i = 0; i < tableOfChildren.size(); i++)
	{
		delete tableOfChildren[i];
	}
}