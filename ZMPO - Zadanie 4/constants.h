#pragma once
#ifndef ZAD3_CONSTANTS_H
#define ZAD3_CONSTANTS_H
#include <string>

const int NO_ERROR = 0;
const int DIVIDING_BY_ZERO_ERROR_CODE = 1;
const int WRONG_FORMULA = 0;
const int DEPTH_LOWER_THAN_0_ERROR_CODE = 2;
const int ERROR_IN_OPENING_FILE = 3;

//Operations
const std::string ADD = "+";
const std::string SUBSTRACT = "-";
const std::string MULTIPLY = "*";
const std::string DIVIDE = "/";
const std::string SINUS = "sin";
const std::string COSINUS = "cos";
const int NUMBER_OF_ARGUMENTS_ADD_SUBSTRACT_MULTIPLY_DIVIDE = 2;
const int NUMBER_OF_ARGUMENTS_SIN_COS = 1;

//Limits for allowed signs in names of variables
const int ASCII_FIRST_DIGIT = 48;
const int ASCII_LAST_DIGIT = 57;

//Helper values
const char SPACE = ' ';
const std::string EMPTY_FORMULA = "";
const std::string ZERO_STRING = "0";
const std::string ONE_STRING = "1";
const std::string DEFAULT_VALUE = "1";
const std::string xString = "x";
const std::string yString = "y";

const int PARSE_NEXT_DIGIT = 10;
const int GO_TO_LAST_EXPRESSION = 2;
const int SECOND_CHILD = 1;
const int FIRST_CHILD = 0;
const int PARSE_MOD = 10;
const char ZERO_CHAR = '0';
const char ONE_CHAR = '1';
const char SEMICOLON = ';';
const char DOT = '.';
const char MINUS = '-';
const char NEW_LINE = '\n';

const int FIRST_ONE_IS_WORSE = 0;
const int NUMBER_OF_AVAIALBE_TYPES_OF_OPERATIONS = 6;
const int NUMBER_OF_AVAIABLE_TYPES_OF_NODES = 3;
const int NUMBER_OF_AVAIABLE_TYPES_OF_LEAVES = 2;
const int NUMBER_OF_AVAIABLE_VARIABLES = 2;
const int CONSTANT_CODE = 0;
const int VARIABLE_CODE = 1;
const int OPERATOR_CODE = 2;
const int SINUS_COSINUS_CODE = 3;
const int MAX_DEPTH = 10;
const int FIRST_ARGUMENT = 0;
const int SECOND_ARGUMENT = 1;
const int THIRD_ARGUMENT = 2;
const int INITIAL_DEPTH = 0;
const int TWO_VARIABLES = 2;

//OPERATION CODES
const int ADD_CODE = 0;
const int SUBSTRACT_CODE = 1;
const int DIVIDE_CODE = 2;
const int MULTIPLY_CODE = 3;
const int SINUS_CODE = 4;
const int COSINUS_CODE = 5;
const int NUMBER_OF_AVAIABLE_TWO_ARGUMENTS_OPERATIONS = 4;

const int DEFAULT_NUMBER_OF_POPULATION = 100;
const int DEFAULT_NUMBER_OF_ITERATIONS = 50;
const int DEFAULT_PERCENTAGE_OF_MUTATION = 30;
const int DEFAULT_PERCENTAGE_OF_PAIRING = 50;

const std::string NAME_OF_FILE_WITH_BEST_TREE = "bestTree.txt";
const std::string NAME_OF_FILE_WITH_DATA = "ZMPO_lista_4_03_sin_x.txt";
#endif //ZAD3_CONSTANTS_H
