#include "Population.h"
#include "DataManager.h"

void main()
{
	CPopulation population(DEFAULT_NUMBER_OF_POPULATION,DEFAULT_NUMBER_OF_ITERATIONS, DEFAULT_PERCENTAGE_OF_MUTATION, DEFAULT_PERCENTAGE_OF_PAIRING);
	CDataManager database;
	if (database.openFile(NAME_OF_FILE_WITH_DATA) == NO_ERROR)
		population.findTheBestExpression(database);
	
}