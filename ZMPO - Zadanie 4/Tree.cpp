#include "Tree.h"

CTree::CTree()
{
	root = NULL;
	treeInPrefixNotation = EMPTY_FORMULA;
}

CTree::CTree(CTree &otherTree)
{
	root = new CNode(*otherTree.root);
	treeInPrefixNotation = EMPTY_FORMULA;
	addPrefixFormula(root);
	evalutedExpression = otherTree.evalutedExpression;
	variables = otherTree.variables;
}

CTree::~CTree()
{
	if (root != NULL)
		delete root;
}

string CTree::print()
{
	return treeInPrefixNotation;
}

int CTree::getNumberOfVariables()
{
	return variables.size();
}

double CTree::getEvalutedExpression()
{
	return evalutedExpression;
}

int CTree::evaluationFunctionFromDatabase(CDataManager database)
{ 
	double sum = 0;
	for (int i = 0; i < database.arrayOfData.size(); i++)
	{
		vector<double> arguments;
		if (variables.size() > 0)
		{
			if (variables.size() == TWO_VARIABLES)
			{
				if (variables[0] == yString && variables[1] == xString)
				{
					arguments.push_back(database.arrayOfData[i][SECOND_ARGUMENT]);
					arguments.push_back(database.arrayOfData[i][FIRST_ARGUMENT]);
				}
				else if (variables[0] == xString && variables[1] == yString )
				{
					arguments.push_back(database.arrayOfData[i][FIRST_ARGUMENT]);
					arguments.push_back(database.arrayOfData[i][SECOND_ARGUMENT]);
				}
			}
			else if (variables[0] == xString)
			{
				arguments.push_back(database.arrayOfData[i][FIRST_ARGUMENT]);
			}
			else
			{
				arguments.push_back(database.arrayOfData[i][SECOND_ARGUMENT]);
			}
		}
		int errorInEvaluation = 0;
		double diffrence = database.arrayOfData[i][THIRD_ARGUMENT] - evaluteResult(arguments, errorInEvaluation);
		if (errorInEvaluation == DIVIDING_BY_ZERO_ERROR_CODE)
		{
			return DIVIDING_BY_ZERO_ERROR_CODE;
		}
		diffrence = diffrence*diffrence;
		sum += diffrence;
	}
	evalutedExpression = sum;
	return NO_ERROR;
}

double CTree::evaluteResult(vector<double> arguments, int &errorInEvaluation)
{
	return evaluationFunction(root, arguments, errorInEvaluation);
}

double CTree::evaluationFunction(CNode* node, vector<double> arguments, int &error)
{
	if (node->value == ADD)
	{
		return evaluationFunction(node->tableOfChildren[FIRST_CHILD], arguments, error) + evaluationFunction(node->tableOfChildren[SECOND_CHILD], arguments, error);
	}
	else if (node->value == SUBSTRACT)
	{
		return evaluationFunction(node->tableOfChildren[FIRST_CHILD], arguments, error) - evaluationFunction(node->tableOfChildren[SECOND_CHILD], arguments, error);
	}
	else if (node->value == MULTIPLY)
	{
		return evaluationFunction(node->tableOfChildren[FIRST_CHILD], arguments, error) * evaluationFunction(node->tableOfChildren[SECOND_CHILD], arguments, error);
	}
	else if (node->value == DIVIDE)
	{
		double firstArgument = evaluationFunction(node->tableOfChildren[FIRST_CHILD], arguments, error);
		double secondArgument = evaluationFunction(node->tableOfChildren[SECOND_CHILD], arguments, error);
		if (secondArgument == 0)
		{
			error = DIVIDING_BY_ZERO_ERROR_CODE;
			return WRONG_FORMULA;
		}
		return firstArgument / secondArgument;
	}
	else if (node->value == SINUS)
	{
		return sin(evaluationFunction(node->tableOfChildren[0], arguments, error));
	}
	else if (node->value == COSINUS)
	{
		return cos(evaluationFunction(node->tableOfChildren[0], arguments, error));
	}
	else if (isNumber(node->value))
		return (double)parseToInt(node->value);
	else
	{
		for (int i = 0; i < variables.size(); i++)
			if (variables[i] == node->value) return (double)arguments[i];
	}
}

void CTree::mutate()
{
	treeInPrefixNotation = EMPTY_FORMULA;
	variables.clear();
	if (root->typeOfNode == CONSTANT_CODE || root->typeOfNode == VARIABLE_CODE)
	{
		delete root;
		root = makeARandomNode(INITIAL_DEPTH);
	}
	else 
		mutateFunction(root);
	addPrefixFormula(root);
	addVariables(root);
}

int CTree::cutTree(int maxDepth)
{
	if (maxDepth < 0)
	{
		return DEPTH_LOWER_THAN_0_ERROR_CODE;
	}
	cutTreeFunction(root, INITIAL_DEPTH, maxDepth);
	variables.clear();
	addVariables(root);
	treeInPrefixNotation = EMPTY_FORMULA;
	addPrefixFormula(root);
	return NO_ERROR;
}

void CTree::cutTreeFunction(CNode* node, int actualDepth, int maxDepth)
{
	if (actualDepth <= maxDepth-1)
	{
		if (node->typeOfNode == OPERATOR_CODE || node->typeOfNode == SINUS_COSINUS_CODE)
		{
			for (int i = 0; i < node->tableOfChildren.size(); i++)
				cutTreeFunction(node->tableOfChildren[i], actualDepth + 1, maxDepth);
		}
	}
	else
	{
		if (node->typeOfNode == OPERATOR_CODE || node->typeOfNode == SINUS_COSINUS_CODE)
		{
			for (int i = 0; i < node->tableOfChildren.size(); i++)
			{
				delete node->tableOfChildren[i];
				node->tableOfChildren[i] = makeARandomLeaf();
			}
		}
	}
}

CNode* CTree::makeARandomLeaf()
{
	int randomizer = rand() % NUMBER_OF_AVAIABLE_TYPES_OF_LEAVES;
	string value;
	if (randomizer == CONSTANT_CODE)
	{
		int intValue = rand() % 100 + 1;
		value = parseToString(intValue);
		return new CNode(value, CONSTANT_CODE);
	}
	else
	{
		int variableRandomizer = rand() % NUMBER_OF_AVAIABLE_VARIABLES;
		if (variableRandomizer == 0)
			value = xString;
		else
			value = yString;
		return new CNode(value, VARIABLE_CODE);
	}
}

void CTree::mutateFunction(CNode* node)
{
	int depthOfNewNode = 0;
	if (node->typeOfNode == OPERATOR_CODE)
	{
		int randomizer = rand() % NUMBER_OF_AVAIABLE_TWO_ARGUMENTS_OPERATIONS;
		if (randomizer == 0)
		{
			if (node->tableOfChildren[FIRST_CHILD]->typeOfNode == CONSTANT_CODE || node->tableOfChildren[FIRST_CHILD]->typeOfNode == VARIABLE_CODE)
			{
				delete node->tableOfChildren[FIRST_CHILD];
				node->tableOfChildren[FIRST_CHILD] = makeARandomNode(depthOfNewNode);
			}
			else 
				mutateFunction(node->tableOfChildren[FIRST_CHILD]);
		}
		else if (randomizer == 1)
		{
			if (node->tableOfChildren[SECOND_CHILD]->typeOfNode == CONSTANT_CODE || node->tableOfChildren[SECOND_CHILD]->typeOfNode == VARIABLE_CODE)
			{
				delete node->tableOfChildren[SECOND_CHILD];
				node->tableOfChildren[SECOND_CHILD] = makeARandomNode(depthOfNewNode);
			}
			else
				mutateFunction(node->tableOfChildren[SECOND_CHILD]);
		}
		else if(randomizer == 2)
		{
			delete node->tableOfChildren[FIRST_CHILD];
			node->tableOfChildren[FIRST_CHILD] = makeARandomNode(depthOfNewNode);
		}
		else
		{
			delete node->tableOfChildren[SECOND_CHILD];
			node->tableOfChildren[SECOND_CHILD] = makeARandomNode(depthOfNewNode);
		}
	}
	else if(node->typeOfNode == SINUS_COSINUS_CODE)
	{
		if (node->tableOfChildren[FIRST_CHILD]->typeOfNode == CONSTANT_CODE || node->tableOfChildren[FIRST_CHILD]->typeOfNode == VARIABLE_CODE)
		{
			delete node->tableOfChildren[FIRST_CHILD];
			node->tableOfChildren[FIRST_CHILD] = makeARandomNode(depthOfNewNode);
		}
		else
		{
			int randomizer = rand() % 2;
			if (randomizer == 0)
			{
				mutateFunction(node->tableOfChildren[FIRST_CHILD]);
			}
			else
			{
				delete node->tableOfChildren[FIRST_CHILD];
				node->tableOfChildren[FIRST_CHILD] = makeARandomNode(depthOfNewNode);
			}
		}
		
	}
}

void CTree::addPrefixFormula(CNode* node)
{
	treeInPrefixNotation += node->value + SPACE;
	for (int i = 0; i < node->tableOfChildren.size(); i++)
		addPrefixFormula(node->tableOfChildren[i]);
}

void CTree::addVariables(CNode* node)
{
	if (node->typeOfNode == VARIABLE_CODE)
	{
		addVariable(node->value);
	}
	for (int i = 0; i < node->tableOfChildren.size(); i++)
		addVariables(node->tableOfChildren[i]);
}

void CTree::pairWithAnotherTree(string &sequence, CNode &nodeToAdd)
{
	CNode* nodeToBeChanged = root;
	while (sequence != EMPTY_FORMULA)
	{
		if (sequence.front() == ZERO_CHAR)
		{
			nodeToBeChanged = nodeToBeChanged->tableOfChildren[FIRST_CHILD];
		}
		else if (sequence.front() == ONE_CHAR)
		{
			nodeToBeChanged = nodeToBeChanged->tableOfChildren[SECOND_CHILD];
		}
		sequence = sequence.substr(1, sequence.length());
	}
	nodeToBeChanged = &nodeToAdd;
	treeInPrefixNotation = EMPTY_FORMULA;
	addPrefixFormula(root);
	variables.clear();
	addVariables(root);
}

CNode* CTree::chooseRandomNode(string &sequence)
{
	CNode* chosenNode = root;
	int randomizer = 0;
	while (randomizer != 2)
	{
		if (chosenNode->typeOfNode == OPERATOR_CODE || chosenNode->typeOfNode == SINUS_COSINUS_CODE)
		{
			randomizer = rand() % NUMBER_OF_AVAIABLE_TYPES_OF_NODES;
			if (randomizer == 0)
			{
				chosenNode = chosenNode->tableOfChildren[FIRST_CHILD];
				sequence += ZERO_STRING;
			}
			else if (randomizer == 1)
			{
				if (chosenNode->typeOfNode == OPERATOR_CODE)
				{
					chosenNode = chosenNode->tableOfChildren[SECOND_CHILD];
					sequence += ONE_STRING;
				}
				else
				{
					chosenNode = chosenNode->tableOfChildren[FIRST_CHILD];
					sequence += ZERO_STRING;
				}
			}
		}
		else
			randomizer = 2;
	}
	return chosenNode;
}

void CTree::buildARandomTree()
{
	if (root != NULL)
	{
		delete root;
	}
	root = makeARandomNode(INITIAL_DEPTH);
	addPrefixFormula(root);
	addVariables(root);
}


CNode* CTree::makeARandomNode(int actualDepth)
{
	int randomizer;
	
	if (actualDepth != MAX_DEPTH)
		randomizer = rand() % NUMBER_OF_AVAIABLE_TYPES_OF_NODES;
	else
		randomizer = rand() % NUMBER_OF_AVAIABLE_TYPES_OF_LEAVES;
	
	string value;
	if (randomizer == OPERATOR_CODE)
	{
		vector<CNode*> children;
		int operationRandomizer = rand() % NUMBER_OF_AVAIALBE_TYPES_OF_OPERATIONS;
		if (operationRandomizer == ADD_CODE) value = ADD;
		else if (operationRandomizer == SUBSTRACT_CODE) value = SUBSTRACT;
		else if (operationRandomizer == DIVIDE_CODE) value = DIVIDE;
		else if (operationRandomizer == MULTIPLY_CODE) value = MULTIPLY;
		else if (operationRandomizer == SINUS_CODE) value = SINUS;
		else if (operationRandomizer == COSINUS_CODE) value = COSINUS;
		if (operationRandomizer < NUMBER_OF_AVAIABLE_TWO_ARGUMENTS_OPERATIONS)
		{
			for (int i = 0; i < NUMBER_OF_ARGUMENTS_ADD_SUBSTRACT_MULTIPLY_DIVIDE; i++)
				children.push_back(makeARandomNode(actualDepth+1));
			return new CNode(value, children,OPERATOR_CODE);
		}
		else
		{
			for (int i = 0; i < NUMBER_OF_ARGUMENTS_SIN_COS; i++)
				children.push_back(makeARandomNode(actualDepth + 1));
			return new CNode(value, children, SINUS_COSINUS_CODE);
		}
	}
	else if (randomizer == CONSTANT_CODE)
	{
		int intValue = rand() % 100 + 1;
		value = parseToString(intValue);
		return new CNode(value,CONSTANT_CODE);
	}
	else if (randomizer == VARIABLE_CODE)
	{
		int variableRandomizer = rand() % 2;
		if (variableRandomizer == 0)
			value = xString;
		else 
			value = yString;
		return new CNode(value,VARIABLE_CODE);
	}
}


void CTree::addVariable(string variable)
{
	bool variableIsAlreadyStored = false;
	for (int i = 0; i < variables.size(); i++)
	{
		if (variables[i] == variable)
			variableIsAlreadyStored = true;
	}
	if (!variableIsAlreadyStored)
	{
		variables.push_back(variable);
	}
}