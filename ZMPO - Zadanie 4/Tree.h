#pragma once
#include "Node.h"
#include "DataManager.h"
#include <string>
#include "constants.h"
#include "tools.h"
#include <stdlib.h>
#include <iostream>

using namespace std;

class CTree
{

public:

	CTree();

	CTree(CTree & otherTree);

	~CTree();

	string print();

	int getNumberOfVariables();

	double getEvalutedExpression();

	int evaluationFunctionFromDatabase(CDataManager database);

	void mutate();

	int cutTree(int maxDepth);

	void cutTreeFunction(CNode* node, int actualDepth, int maxDepth);

	CNode* makeARandomLeaf();
	
	void pairWithAnotherTree(string &sequence, CNode &nodeToAdd);

	CNode* chooseRandomNode(string &sequence);

	void buildARandomTree();

private:
	double evaluteResult(vector<double> arguments, int &errorInEvaluation);

	double evaluationFunction(CNode * node, vector<double> arguments, int &error);

	void mutateFunction(CNode * node);

	void addVariables(CNode* node);

	void addVariable(string variable);

	CNode* makeARandomNode(int actualDepth);

	void addPrefixFormula(CNode* node);

	CNode* root;
	vector<string> variables;
	string treeInPrefixNotation;
	double evalutedExpression;
};

